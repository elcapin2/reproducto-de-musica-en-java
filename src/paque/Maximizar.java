package paque;

import javax.swing.ImageIcon;
//import javax.swing.JFrame;

public class Maximizar extends javax.swing.JFrame {

    private String rutaope;
    
    public Maximizar() {
        initComponents();
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        //frame.setDefaultCloseOperation(JFrame.Do_NOTHING_ON_CLOSE);
        setResizable(false);
        //verImagen();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        MostrarImagen = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        MostrarImagen.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        MostrarImagen.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 5));
        getContentPane().add(MostrarImagen, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 750, 550));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/apollo__god_of_music_1.png"))); // NOI18N
        jLabel1.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 5, true));
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 800, 600));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Maximizar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Maximizar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Maximizar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Maximizar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Maximizar().setVisible(true);
            }
        });
    }
    
    public void verImagen(String rr){
        rutaope=rr;
        if(rutaope!=""){
        ImageIcon MostrarImagen=new ImageIcon(rutaope);
        int ancho=MostrarImagen.getIconWidth();
        int alto=MostrarImagen.getIconHeight();
        
        this.MostrarImagen.setSize(ancho, alto);
        this.MostrarImagen.setIcon(MostrarImagen); 
        System.out.println("Mostrar ruta: "+rutaope);
        System.out.println("HOLLLLLAAALALALALALALAALAL");
        }        
    }
    
    public String getRutaope(){
        return rutaope;
    }
    
    public void setRutaope(String rutaope){
        this.rutaope=rutaope;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel MostrarImagen;
    private javax.swing.JLabel jLabel1;
    // End of variables declaration//GEN-END:variables
}
