package paquete;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;


public class FMain {
    
    FileInputStream Entrada;//Archivo de puntero
    BufferedInputStream BuferEntrada;//Puntero en Bufer
    //public boolean;
    
    public Player Reproducir;//Declaramos el objeto de tipo reproducir
    
    public long pausalocal;//Guarda
    //public long tamtotalcancion;
    public long tamtotalcan;
    
    public String localizaciondirectorio;
    public boolean canfin;

    public FMain() {
        this.canfin = false;
    }
    
    /*public boolean getCanfin(){
        canfin=true;
        return canfin;
    }*/
    
    public void stop(){
    if(Reproducir != null){
        Reproducir.close();
        pausalocal=0;
        tamtotalcan=0;
        //tamtotalcancion=0;
        //System.out.println("pausa: "+pausalocal);
        //System.out.println("total cancion: "+tamtotalcan);
        //System.out.println("total cancion 2: "+tamtotalcancion);
        
    }   
    }
    
    public void Pausa(){
    if(Reproducir != null){
        try {
            File Acceso=new File(localizaciondirectorio);
            pausalocal=Entrada.available();
            Reproducir.close();
            tamtotalcan=Acceso.length();
            //System.out.println("pausa Pausa: "+pausalocal);
            //System.out.println("total cancion Pausa: "+tamtotalcan);
            //System.out.println("total cancion 2Pausa: "+tamtotalcancion);
        } catch (IOException ex) {
            
        }
    }   
    }
    
    public void Reanudar (){
       
        try {
            
            Entrada=new FileInputStream(localizaciondirectorio);
            BuferEntrada= new BufferedInputStream(Entrada);
            File Acceso=new File(localizaciondirectorio);
            
            Reproducir = new Player(BuferEntrada);
            
            Entrada.skip(tamtotalcan-pausalocal);
            //System.out.println("pausa Reanudar: "+pausalocal);
            //System.out.println("total cancion Reanudar: "+tamtotalcan);
            //System.out.println("total cancion 2Reanudar: "+tamtotalcancion);
            if(pausalocal==Acceso.length()){
            canfin=true;
            }
        } catch (FileNotFoundException | JavaLayerException ex) {
            
        } catch (IOException ex) {
            Logger.getLogger(FMain.class.getName()).log(Level.SEVERE, null, ex);
        } 
        
        new Thread(){
        
        @Override
        public void run(){
        try {
            Reproducir.play();
        } catch (JavaLayerException ex) {
            Logger.getLogger(FMain.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
    }.start();
    }
        
    
   
        
    
    public void Play (String path){
        try {
            Entrada=new FileInputStream(path);
            BuferEntrada= new BufferedInputStream(Entrada);
            
            
            Reproducir = new Player(BuferEntrada);
            
            
            //System.out.println("pausa PLAY: "+pausalocal);
            //System.out.println("total cancion PLAY: "+tamtotalcan);
            //System.out.println("total cancion 2PLAY: "+tamtotalcancion);
            
            localizaciondirectorio = path + "";
            
            } catch (FileNotFoundException | JavaLayerException ex) {
            
            } catch (IOException ex) {
            
            }
    
    new Thread(){
        
        @Override
        public void run(){
        try {
            Reproducir.play();
        } catch (JavaLayerException ex) {
            Logger.getLogger(FMain.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
    }.start();
    }
}
