package paquete;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

public class FMain2 {
    FileInputStream FS;
    BufferedInputStream BS; 
    
    
    public Player player;
    public long pausaloc;
    
    public void Stop(){
        
        if(player !=null){
            player.close();
        }
    }
    
    public void Pausa(){
        
        if(player !=null){
            try {
                pausaloc=FS.available();
                player.close();
            } catch (IOException ex) {
                
            }
        }
    }
    
    public void Play(String path){
        try { 
            FS =new FileInputStream(path);
            BS= new BufferedInputStream(FS);
            
            player = new Player(BS);
        } catch (FileNotFoundException | JavaLayerException ex) {
            
        }
        
        new Thread(){
            
            @Override
            public void run(){
                
                try {
                    player.play();
                } catch (JavaLayerException ex) {
                    
                }
            }
        }.start();
        
    }
    
    
    
    
    
     
    
    
    
}
